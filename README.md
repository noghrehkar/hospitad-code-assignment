# How to Run Application

### Database Configuration

To run the application you should change database connection information in application.properties in resources and flyway.properties in root directory and use mysql as your database
engine.Then run command "mvn flyway:migrate" in project root, now your database is ready to use.

### Default Address

application will run on localhost:8080 by default.

# Test API's

All RESTApi has functional test, you can find them in test/java/com/example/hospitad/restapi
