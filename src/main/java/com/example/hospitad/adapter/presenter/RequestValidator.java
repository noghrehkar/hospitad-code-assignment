package com.example.hospitad.adapter.presenter;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

@Aspect
@Service
public class RequestValidator {

  @Autowired ResponseMessageJsonPresenter responseMessageJsonPresenter;

  /**
   * this method will run before every POST and PUT rest api to validate request body
   *
   * @param joinPoint
   * @param errors
   * @return
   * @throws Throwable
   */
  @Around("execution(* com.example.hospitad.adapter.restapi..*(..)) && args(..,errors)")
  public String validateData(ProceedingJoinPoint joinPoint, Errors errors) throws Throwable {
    if (errors.hasErrors()) {
      return responseMessageJsonPresenter.getJson(
          HttpStatus.BAD_REQUEST.value(),
          generateFieldErrorMessages(errors),
          ResourceBundleUtil.getMessage("general.validation_error"));
    } else {
      return (String) joinPoint.proceed();
    }
  }

  private Map generateFieldErrorMessages(Errors errors) {
    Map errorMessages = new HashMap();
    for (FieldError error : errors.getFieldErrors()) {
      errorMessages.put(error.getField(), error.getDefaultMessage());
    }
    return errorMessages;
  }
}
