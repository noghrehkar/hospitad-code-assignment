package com.example.hospitad.adapter.presenter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResponseMessageJsonPresenter {

  @Autowired private ObjectMapper jacksonJSONConverter;

  int status;
  Object data;
  String message;

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getJson(int status, Object data, String message) throws JsonProcessingException {
    this.status = status;
    this.data = data;
    this.message = message;
    return jacksonJSONConverter.writeValueAsString(this);
  }
}
