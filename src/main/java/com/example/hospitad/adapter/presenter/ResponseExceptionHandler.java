package com.example.hospitad.adapter.presenter;

import com.example.hospitad.usecase.exception.BaseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ValidationException;

@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

  @Autowired ResponseMessageJsonPresenter responseMessageJsonPresenter;

  /**
   * Handle all validation exception in GET and DELETE RESTApi's
   *
   * @param ex
   * @param request
   * @return
   * @throws JsonProcessingException
   */
  @ExceptionHandler(value = {ValidationException.class})
  public ResponseEntity<Object> handleValidationException(Exception ex, WebRequest request)
      throws JsonProcessingException {
    return generateHandledMessageForException(ex, request);
  }

  /**
   * Handle all exception that extends BaseException
   *
   * @param ex
   * @param request
   * @return
   * @throws JsonProcessingException
   */
  @ExceptionHandler(value = {BaseException.class})
  public ResponseEntity<Object> handleBaseException(Exception ex, WebRequest request)
      throws JsonProcessingException {
    return generateHandledMessageForException(ex, request);
  }

  /**
   * Handle all general exceptions
   *
   * @param ex
   * @param request
   * @return
   * @throws JsonProcessingException
   */
  @ExceptionHandler(value = {Exception.class})
  public ResponseEntity<Object> handleGeneralException(Exception ex, WebRequest request)
      throws JsonProcessingException {
    String bodyOfResponse =
        responseMessageJsonPresenter.getJson(
            HttpStatus.NOT_ACCEPTABLE.value(),
            null,
            ResourceBundleUtil.getMessage("general.error"));
    return handleExceptionInternal(
        ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE, request);
  }

  private ResponseEntity<Object> generateHandledMessageForException(
      Exception ex, WebRequest request) throws JsonProcessingException {
    String bodyOfResponse = ex.getLocalizedMessage();
    return handleExceptionInternal(
        ex,
        responseMessageJsonPresenter.getJson(
            HttpStatus.NOT_ACCEPTABLE.value(), null, bodyOfResponse),
        new HttpHeaders(),
        HttpStatus.NOT_ACCEPTABLE,
        request);
  }
}
