package com.example.hospitad.adapter.viewmodel;

import com.example.hospitad.usecase.exception.PasswordNotMatch;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserViewModel {

  Long id;

  @NotNull @NotEmpty String name;

  @NotNull @NotEmpty String username;

  @NotNull @NotEmpty String password;

  @NotNull @NotEmpty String matchingPassword;

  @NotNull CustomerViewModel customer;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getMatchingPassword() {
    return matchingPassword;
  }

  public void setMatchingPassword(String matchingPassword) throws PasswordNotMatch {
    this.matchingPassword = matchingPassword;
  }

  public CustomerViewModel getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerViewModel customer) {
    this.customer = customer;
  }
}
