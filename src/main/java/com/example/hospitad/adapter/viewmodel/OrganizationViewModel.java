package com.example.hospitad.adapter.viewmodel;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

class OrganizationInnerViewModel {
  Long id;
  String name;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

public class OrganizationViewModel {

  Long id;

  @NotNull @NotEmpty String name;

  UserResponseViewModel creator;

  CustomerViewModel customer;

  OrganizationInnerViewModel parent;

  List<OrganizationInnerViewModel> children;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public UserResponseViewModel getCreator() {
    return creator;
  }

  public void setCreator(UserResponseViewModel creator) {
    this.creator = creator;
  }

  public CustomerViewModel getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerViewModel customer) {
    this.customer = customer;
  }

  public OrganizationInnerViewModel getParent() {
    return parent;
  }

  public void setParent(OrganizationInnerViewModel parent) {
    this.parent = parent;
  }

  public List<OrganizationInnerViewModel> getChildren() {
    return children;
  }

  public void setChildren(List<OrganizationInnerViewModel> children) {
    this.children = children;
  }
}
