package com.example.hospitad.adapter.viewmodel;

public class OrganizationEditViewModel {

  Long id;
  String name;
  OrganizationViewModel parent;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OrganizationViewModel getParent() {
    return parent;
  }

  public void setParent(OrganizationViewModel parent) {
    this.parent = parent;
  }
}
