package com.example.hospitad.adapter.viewmodel;

public class CustomerViewModel {

  Long id;
  String CompanyName;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCompanyName() {
    return CompanyName;
  }

  public void setCompanyName(String companyName) {
    CompanyName = companyName;
  }
}
