package com.example.hospitad.adapter.restapi;

import com.example.hospitad.entity.User;
import com.example.hospitad.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BaseController {

  @Autowired UserRepository userRepository;

  /** @return current authenticated user */
  protected User getCurrentUser() {
    UserDetails currentAuthUser =
        (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    Optional<User> currentUser = userRepository.findByUsername(currentAuthUser.getUsername());
    return currentUser.get();
  }
}
