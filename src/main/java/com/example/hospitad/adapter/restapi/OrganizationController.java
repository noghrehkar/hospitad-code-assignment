package com.example.hospitad.adapter.restapi;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;
import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.example.hospitad.adapter.viewmodel.OrganizationEditViewModel;
import com.example.hospitad.adapter.viewmodel.OrganizationViewModel;
import com.example.hospitad.entity.Organization;
import com.example.hospitad.entity.User;
import com.example.hospitad.persistence.repository.UserRepository;
import com.example.hospitad.usecase.exception.*;
import com.example.hospitad.usecase.organization.CreateOrganization;
import com.example.hospitad.usecase.organization.DeleteOrganization;
import com.example.hospitad.usecase.organization.EditOrganization;
import com.example.hospitad.usecase.organization.GetOrganization;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@Validated
@RequestMapping("/organization")
@RestController
public class OrganizationController extends BaseController {

  @Autowired ModelMapper modelMapper;

  @Autowired ResponseMessageJsonPresenter responseMessageJsonPresenter;

  @Autowired CreateOrganization createOrganization;

  @Autowired DeleteOrganization deleteOrganization;

  @Autowired UserRepository userRepository;

  @Autowired GetOrganization getOrganization;

  @Autowired EditOrganization editOrganization;

  @PostMapping("/")
  public @ResponseBody String create(
      @RequestBody @Valid OrganizationViewModel organizationData, Errors errors)
      throws JsonProcessingException, UserNotExist, ParentNotExist, OrganizationExist,
          CustomerNotExist {

    Organization newOrganization = modelMapper.map(organizationData, Organization.class);
    User currentUser = getCurrentUser();
    newOrganization.setCreator(currentUser);
    newOrganization = createOrganization.createIfNotExist(newOrganization);

    OrganizationViewModel createdOrganization =
        modelMapper.map(newOrganization, OrganizationViewModel.class);
    return responseMessageJsonPresenter.getJson(
        HttpStatus.ACCEPTED.value(),
        createdOrganization,
        ResourceBundleUtil.getMessage("organization.create_successful"));
  }

  @DeleteMapping("/{id}")
  public @ResponseBody String delete(@PathVariable Long id)
      throws JsonProcessingException, OrganizationNotExist, UserNotAllowed {

    User currentUser = getCurrentUser();
    deleteOrganization.deleteByIdIfExist(id, currentUser);

    return responseMessageJsonPresenter.getJson(
        HttpStatus.ACCEPTED.value(),
        null,
        ResourceBundleUtil.getMessage("organization.delete_successful"));
  }

  @GetMapping("")
  public @ResponseBody String getAll(
      @RequestParam(value = "pageNumber", required = false, defaultValue = "1") @Min(1)
          int pageNumber)
      throws JsonProcessingException {

    List<Organization> organizations = getOrganization.getOrganizationListByPageNumber(pageNumber);
    OrganizationViewModel[] organizationList =
        modelMapper.map(organizations, OrganizationViewModel[].class);

    return responseMessageJsonPresenter.getJson(
        HttpStatus.ACCEPTED.value(), organizationList, null);
  }

  @GetMapping("/{organizationId}/children")
  public @ResponseBody String getChildren(
      @PathVariable Long organizationId,
      @RequestParam(value = "pageNumber", defaultValue = "1") @Min(1) int pageNumber)
      throws JsonProcessingException, OrganizationNotExist, InvalidPageNumber {

    List<Organization> organizations =
        getOrganization.getOrganizationChildrenListByPageNumber(organizationId, pageNumber);
    OrganizationViewModel[] organizationList =
        modelMapper.map(organizations, OrganizationViewModel[].class);

    return responseMessageJsonPresenter.getJson(
        HttpStatus.ACCEPTED.value(), organizationList, null);
  }

  @PutMapping("/{organizationId}")
  public @ResponseBody String edit(
      @RequestBody OrganizationEditViewModel organizationChangedData,
      @PathVariable Long organizationId,
      Errors errors)
      throws JsonProcessingException, OrganizationNotExist, OrganizationExist,
          ParentAndChildIsSame {

    Organization newOrganizationData = modelMapper.map(organizationChangedData, Organization.class);
    editOrganization.changeInformation(organizationId, newOrganizationData);

    return responseMessageJsonPresenter.getJson(
        HttpStatus.ACCEPTED.value(),
        null,
        ResourceBundleUtil.getMessage("organization.edit_successful"));
  }
}
