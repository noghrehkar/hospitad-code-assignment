package com.example.hospitad.adapter.restapi;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;
import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/login")
@RestController
public class LoginController {

  @Autowired ResponseMessageJsonPresenter responseMessageJsonPresenter;

  @GetMapping("/successful")
  public @ResponseBody String successful() throws JsonProcessingException {
    return responseMessageJsonPresenter.getJson(
        HttpStatus.ACCEPTED.value(), null, ResourceBundleUtil.getMessage("user.login_successful"));
  }

  @GetMapping("/failure")
  public @ResponseBody String failure() throws JsonProcessingException {
    return responseMessageJsonPresenter.getJson(
        HttpStatus.BAD_REQUEST.value(), null, ResourceBundleUtil.getMessage("user.login_failure"));
  }
}
