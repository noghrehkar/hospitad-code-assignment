package com.example.hospitad.adapter.restapi;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;
import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.example.hospitad.adapter.viewmodel.UserResponseViewModel;
import com.example.hospitad.adapter.viewmodel.UserViewModel;
import com.example.hospitad.entity.User;
import com.example.hospitad.usecase.exception.CustomerExist;
import com.example.hospitad.usecase.exception.PasswordNotMatch;
import com.example.hospitad.usecase.exception.UserExist;
import com.example.hospitad.usecase.user.CreateUser;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/user")
@RestController
public class UserController extends BaseController {

  @Autowired CreateUser createUser;

  @Autowired ModelMapper modelMapper;

  @Autowired ResponseMessageJsonPresenter responseMessageJsonPresenter;

  @PostMapping("/register")
  public @ResponseBody String registerUser(
      @RequestBody @Valid UserViewModel userData, Errors errors)
      throws JsonProcessingException, PasswordNotMatch, UserExist, CustomerExist {

    User newUser = modelMapper.map(userData, User.class);
    if (userData.getPassword().equals(userData.getMatchingPassword()) == false) {
      throw new PasswordNotMatch();
    }
    User createdUser = createUser.createIfNotExist(newUser);
    UserResponseViewModel createdUserResponse =
        modelMapper.map(createdUser, UserResponseViewModel.class);

    return responseMessageJsonPresenter.getJson(
        HttpStatus.ACCEPTED.value(),
        createdUserResponse,
        ResourceBundleUtil.getMessage("user.register_successful"));
  }
}
