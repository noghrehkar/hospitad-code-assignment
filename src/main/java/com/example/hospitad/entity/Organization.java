package com.example.hospitad.entity;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@Where(clause = "deleted_at is null")
@Entity
@SQLDelete(sql = "UPDATE organization SET deleted_at=current_time() WHERE id=?")
public class Organization extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;

  String name;

  @ManyToOne(targetEntity = User.class)
  User creator;

  @ManyToOne(targetEntity = Customer.class)
  Customer customer;

  @ManyToOne(targetEntity = Organization.class)
  Organization parent;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
  Set<Organization> children;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public User getCreator() {
    return creator;
  }

  public void setCreator(User creator) {
    this.creator = creator;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Organization getParent() {
    return parent;
  }

  public void setParent(Organization parent) {
    this.parent = parent;
  }

  public Set<Organization> getChildren() {
    return children;
  }

  public void setChildren(Set<Organization> children) {
    this.children = children;
  }
}
