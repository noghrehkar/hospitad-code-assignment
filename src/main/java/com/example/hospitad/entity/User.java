package com.example.hospitad.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class User extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;

  String name;
  String username;
  String password;

  @OneToOne Customer customer;

  @OneToMany(mappedBy = "creator")
  Set<Organization> organizations;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Set<Organization> getOrganizations() {
    return organizations;
  }

  public void setOrganizations(Set<Organization> organizations) {
    this.organizations = organizations;
  }
}
