package com.example.hospitad.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Customer extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id;

  String companyName;

  @OneToOne(mappedBy = "customer")
  User user;

  @OneToMany(mappedBy = "customer")
  Set<Organization> organizations;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Set<Organization> getOrganizations() {
    return organizations;
  }

  public void setOrganizations(Set<Organization> organizations) {
    this.organizations = organizations;
  }
}
