package com.example.hospitad.usecase.user;

import com.example.hospitad.entity.Organization;
import com.example.hospitad.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.OrganizationRepository;

@Service
public class CheckUserPermission {

  @Autowired OrganizationRepository organizationRepository;

  public boolean hasDeleteOrganizationPermission(Organization organization, User user) {

    if (organization.getCreator().getId() == user.getId()) {
      return true;
    } else {
      return false;
    }
  }
}
