package com.example.hospitad.usecase.user;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;
import com.example.hospitad.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.UserRepository;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Optional;

/**
 * implement UserDetailsService interface to change spring security checking credential approach
 * with this customization spring security will check credential from database
 */
@Service
@Transactional
public class UserAuthenticationService
    implements org.springframework.security.core.userdetails.UserDetailsService {

  @Autowired private UserRepository userRepository;

  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    boolean enabled = true;
    boolean accountNonExpired = true;
    boolean credentialsNonExpired = true;
    boolean accountNonLocked = true;

    Optional<User> user = userRepository.findByUsername(username);
    if (user.isEmpty()) {
      throw new UsernameNotFoundException(
          ResourceBundleUtil.getMessage("user.not_exist") + username);
    } else {
      User foundUser = user.get();
      return new org.springframework.security.core.userdetails.User(
          foundUser.getUsername(),
          foundUser.getPassword(),
          enabled,
          accountNonExpired,
          credentialsNonExpired,
          accountNonLocked,
          new ArrayList<>());
    }
  }
}
