package com.example.hospitad.usecase.user;

import com.example.hospitad.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.UserRepository;
import com.example.hospitad.usecase.exception.UserNotExist;

import java.util.Optional;

@Service
public class GetUser {

  @Autowired UserRepository userRepository;

  public User getById(Long id) throws UserNotExist {

    Optional<User> foundUser = userRepository.findById(id);
    if (foundUser.isEmpty()) {
      throw new UserNotExist();
    } else {
      return foundUser.get();
    }
  }
}
