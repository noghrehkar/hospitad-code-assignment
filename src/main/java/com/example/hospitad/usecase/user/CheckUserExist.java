package com.example.hospitad.usecase.user;

import com.example.hospitad.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.UserRepository;

import java.util.Optional;

@Service
public class CheckUserExist {

  @Autowired UserRepository userRepository;

  public boolean isExistByUsername(String username) {

    Optional<User> user = userRepository.findByUsername(username);
    if (user.isEmpty()) {
      return false;
    } else {
      return true;
    }
  }
}
