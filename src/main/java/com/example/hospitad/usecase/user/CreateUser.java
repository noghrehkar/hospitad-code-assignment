package com.example.hospitad.usecase.user;

import com.example.hospitad.entity.Customer;
import com.example.hospitad.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.CustomerRepository;
import com.example.hospitad.persistence.repository.UserRepository;
import com.example.hospitad.usecase.customer.CreateCustomer;
import com.example.hospitad.usecase.exception.CustomerExist;
import com.example.hospitad.usecase.exception.UserExist;

@Service
public class CreateUser {

  @Autowired UserRepository userRepository;

  @Autowired CustomerRepository customerRepository;

  @Autowired CheckUserExist checkUserExist;

  @Autowired CreateCustomer createCustomer;

  @Autowired PasswordEncoder bCryptPasswordEncoder;

  public User createIfNotExist(User userData) throws UserExist, CustomerExist {

    if (checkUserExist.isExistByUsername(userData.getUsername())) {
      throw new UserExist();
    }
    Customer customer = createCustomer.createIfNotExist(userData.getCustomer());
    User newUser = new User();
    newUser.setName(userData.getName());
    String encryptedPassword = bCryptPasswordEncoder.encode(userData.getPassword());
    newUser.setPassword(encryptedPassword);
    newUser.setCustomer(customer);
    newUser.setName(userData.getName());
    newUser.setUsername(userData.getUsername());
    newUser = userRepository.save(newUser);
    return newUser;
  }
}
