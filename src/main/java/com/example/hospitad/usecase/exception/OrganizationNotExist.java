package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class OrganizationNotExist extends BaseException {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("organization.not_exist");
  }
}
