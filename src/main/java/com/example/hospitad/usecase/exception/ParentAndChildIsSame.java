package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class ParentAndChildIsSame extends BaseException {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("organization.parent_child_is_same");
  }
}
