package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class ParentNotExist extends OrganizationNotExist {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("organization.parent.not_exist");
  }
}
