package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class CustomerNotExist extends BaseException {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("customer.not_exist");
  }
}
