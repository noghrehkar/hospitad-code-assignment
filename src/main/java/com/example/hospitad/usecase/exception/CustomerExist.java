package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class CustomerExist extends BaseException {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("customer.exist");
  }
}
