package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class UserNotExist extends BaseException {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("user.not_exist");
  }
}
