package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class PasswordNotMatch extends BaseException {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("user.password_not_match");
  }
}
