package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class InvalidPageNumber extends BaseException {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("organization.invalid_page_number");
  }
}
