package com.example.hospitad.usecase.exception;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;

public class OrganizationExist extends BaseException {
  @Override
  public String getMessage() {
    return ResourceBundleUtil.getMessage("organization.exist");
  }
}
