package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Organization;
import com.example.hospitad.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.OrganizationRepository;
import com.example.hospitad.usecase.exception.OrganizationNotExist;
import com.example.hospitad.usecase.exception.UserNotAllowed;
import com.example.hospitad.usecase.user.CheckUserPermission;

@Service
public class DeleteOrganization {

  @Autowired OrganizationRepository organizationRepository;

  @Autowired CheckOrganizationExist checkOrganizationExist;

  @Autowired CheckUserPermission checkUserPermission;

  public void deleteByIdIfExist(Long id, User user) throws OrganizationNotExist, UserNotAllowed {

    if (checkOrganizationExist.isExistById(id)) {
      Organization organization = organizationRepository.findById(id).get();
      if (checkUserPermission.hasDeleteOrganizationPermission(organization, user)) {
        organizationRepository.delete(organization);
      } else {
        throw new UserNotAllowed();
      }
    } else {
      throw new OrganizationNotExist();
    }
  }
}
