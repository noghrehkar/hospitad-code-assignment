package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.OrganizationRepository;
import com.example.hospitad.usecase.exception.OrganizationExist;
import com.example.hospitad.usecase.exception.OrganizationNotExist;
import com.example.hospitad.usecase.exception.ParentAndChildIsSame;
import com.example.hospitad.usecase.exception.ParentNotExist;
import java.util.Optional;

@Service
public class EditOrganization {

  @Autowired CheckOrganizationExist checkOrganizationExist;

  @Autowired OrganizationRepository organizationRepository;

  public void changeInformation(Long organizationId, Organization newOrganizationData)
      throws OrganizationNotExist, OrganizationExist, ParentAndChildIsSame {

    if (checkOrganizationExist.isExistById(organizationId) == false) {
      throw new OrganizationNotExist();
    }
    Organization foundOrganization = organizationRepository.findById(organizationId).get();

    if (newOrganizationData.getName() != null) {
      if (checkOrganizationExist.isExistByCustomerIdAndName(
          foundOrganization.getCustomer().getId(), newOrganizationData.getName())) {
        throw new OrganizationExist();
      } else {
        foundOrganization.setName(newOrganizationData.getName());
      }
    }

    if (newOrganizationData.getParent() != null) {
      if (checkOrganizationExist.isExistByCustomerIdAndName(
              foundOrganization.getCustomer().getId(), newOrganizationData.getParent().getName())
          == false) {
        throw new ParentNotExist();
      } else {
        Optional<Organization> newParent =
            organizationRepository.findByCustomerIdAndName(
                foundOrganization.getCustomer().getId(), newOrganizationData.getParent().getName());
        if (newParent.get().getId().equals(foundOrganization.getId())) {
          throw new ParentAndChildIsSame();
        }
        if (checkOrganizationExist.isNewParentMakeCycleInOrganizationStructure(
                newOrganizationData.getParent().getId(), foundOrganization.getId())
            == false) {
          foundOrganization.setParent(newParent.get());
        }
      }
    }
    organizationRepository.save(foundOrganization);
  }
}
