package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.OrganizationRepository;
import com.example.hospitad.usecase.exception.InvalidPageNumber;
import com.example.hospitad.usecase.exception.OrganizationNotExist;

import java.util.List;
import java.util.Optional;

@Service
public class GetOrganization {

  final int Page_SIZE = 10;

  @Autowired OrganizationRepository organizationRepository;

  @Autowired CheckOrganizationExist checkOrganizationExist;

  public Organization getById(Long id) throws OrganizationNotExist {
    Optional<Organization> foundOrganization = organizationRepository.findById(id);
    if (foundOrganization.isEmpty()) {
      throw new OrganizationNotExist();
    } else {
      return foundOrganization.get();
    }
  }

  public List<Organization> getOrganizationListByPageNumber(int pageNumber) {
    return organizationRepository.findAll(PageRequest.of(pageNumber - 1, Page_SIZE)).toList();
  }

  public List<Organization> getOrganizationChildrenListByPageNumber(
      Long organizationId, int pageNumber) throws OrganizationNotExist, InvalidPageNumber {
    if (checkOrganizationExist.isExistById(organizationId) == false) {
      throw new OrganizationNotExist();
    } else {

      if (pageNumber <= 0) {
        throw new InvalidPageNumber();
      }
      return organizationRepository.getByParentId(
          organizationId, PageRequest.of(pageNumber - 1, Page_SIZE));
    }
  }
}
