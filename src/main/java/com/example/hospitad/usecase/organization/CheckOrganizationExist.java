package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.OrganizationRepository;

import java.util.Optional;

@Service
public class CheckOrganizationExist {

  @Autowired OrganizationRepository organizationRepository;

  public boolean isExistByCustomerIdAndName(Long customerId, String name) {

    Optional<Organization> foundOrganization =
        organizationRepository.findByCustomerIdAndName(customerId, name);
    if (foundOrganization.isEmpty()) {
      return false;
    } else {
      return true;
    }
  }

  public boolean isExistById(Long id) {

    Optional<Organization> foundOrganization = organizationRepository.findById(id);
    if (foundOrganization.isEmpty()) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * TODO: in future development we can check whether new parent make cycle problem in organization
   * structure or not for now we will ignore this check and always return false
   *
   * @param newParentId
   * @param organizationId
   * @return
   */
  public boolean isNewParentMakeCycleInOrganizationStructure(
      Long newParentId, Long organizationId) {
    return false;
  }
}
