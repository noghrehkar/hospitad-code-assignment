package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.OrganizationRepository;
import com.example.hospitad.usecase.customer.GetCustomer;
import com.example.hospitad.usecase.user.GetUser;
import com.example.hospitad.usecase.exception.*;

import java.util.HashSet;

@Service
public class CreateOrganization {

  @Autowired CheckOrganizationExist checkOrganizationExist;

  @Autowired GetOrganization getOrganization;

  @Autowired OrganizationRepository organizationRepository;

  @Autowired GetCustomer getCustomer;

  @Autowired GetUser getUser;

  public Organization createIfNotExist(Organization organizationData)
      throws ParentNotExist, OrganizationExist, CustomerNotExist, UserNotExist {

    if (checkOrganizationExist.isExistByCustomerIdAndName(
        organizationData.getCreator().getCustomer().getId(), organizationData.getName())) {
      throw new OrganizationExist();
    }
    Organization parent = null;
    Organization newOrganization = new Organization();
    if (organizationData.getParent() != null && organizationData.getParent().getId() != null) {
      try {
        parent = getOrganization.getById(organizationData.getParent().getId());
      } catch (OrganizationNotExist organizationNotExist) {
        throw new ParentNotExist();
      }
    }

    newOrganization.setCustomer(
        getCustomer.getById(organizationData.getCreator().getCustomer().getId()));
    newOrganization.setParent(parent);
    newOrganization.setName(organizationData.getName());
    newOrganization.setCreator(getUser.getById(organizationData.getCreator().getId()));
    newOrganization.setChildren(new HashSet<>());

    return organizationRepository.save(newOrganization);
  }
}
