package com.example.hospitad.usecase.customer;

import com.example.hospitad.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.CustomerRepository;
import com.example.hospitad.usecase.exception.CustomerNotExist;

import java.util.Optional;

@Service
public class GetCustomer {

  @Autowired CustomerRepository customerRepository;

  public Customer getById(Long customerId) throws CustomerNotExist {
    if (customerId == null) {
      throw new CustomerNotExist();
    } else {
      Optional<Customer> foundCustomer = customerRepository.findById(customerId);
      if (foundCustomer.isEmpty()) {
        throw new CustomerNotExist();
      } else {
        return foundCustomer.get();
      }
    }
  }
}
