package com.example.hospitad.usecase.customer;

import com.example.hospitad.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.CustomerRepository;

import java.util.Optional;

@Service
public class CheckCustomerExist {

  @Autowired CustomerRepository customerRepository;

  public boolean isExistByCompanyName(String companyName) {
    Optional<Customer> customer = customerRepository.findByCompanyName(companyName);
    if (customer.isEmpty()) {
      return false;
    } else {
      return true;
    }
  }
}
