package com.example.hospitad.usecase.customer;

import com.example.hospitad.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.hospitad.persistence.repository.CustomerRepository;
import com.example.hospitad.usecase.exception.CustomerExist;

@Service
public class CreateCustomer {

  @Autowired CustomerRepository customerRepository;

  @Autowired CheckCustomerExist checkCustomerExist;

  public Customer createIfNotExist(Customer newCustomerData) throws CustomerExist {
    Customer newCustomer = new Customer();
    if (checkCustomerExist.isExistByCompanyName(newCustomerData.getCompanyName())) {
      throw new CustomerExist();
    } else {
      newCustomer.setCompanyName(newCustomerData.getCompanyName());
      newCustomer = customerRepository.save(newCustomer);
      return newCustomer;
    }
  }
}
