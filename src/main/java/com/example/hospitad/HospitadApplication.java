package com.example.hospitad;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@EntityScan(basePackages = {"com.example.hospitad.entity"})
@EnableJpaRepositories(basePackages = "com.example.hospitad.persistence.repository")
@ComponentScan(
    basePackages = {
      "com.example.hospitad.adapter",
      "com.example.hospitad.usecase",
      "com.example.hospitad.configuration"
    })
@SpringBootApplication
@EnableWebSecurity
public class HospitadApplication {

  public static void main(String[] args) {
    SpringApplication.run(HospitadApplication.class, args);
  }

  @Bean
  public ModelMapper getModelMapper() {
    return new ModelMapper();
  }
}
