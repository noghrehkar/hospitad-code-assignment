package com.example.hospitad.persistence.repository;

import com.example.hospitad.entity.Organization;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrganizationRepository extends PagingAndSortingRepository<Organization, Long> {

  Optional<Organization> findByCustomerIdAndName(Long customerId, String name);

  List<Organization> getByParentId(Long id, Pageable pageable);
}
