package com.example.hospitad.persistence.repository;

import com.example.hospitad.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {

  Optional<Customer> findByCompanyName(String companyName);
}
