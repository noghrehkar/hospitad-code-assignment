DROP
ALL OBJECTS;
SET
MODE MYSQL;

CREATE TABLE `customer`
(
    `id`           bigint(20) NOT NULL AUTO_INCREMENT,
    `company_name` varchar(255) DEFAULT NULL,
    `created_at`   datetime(6) DEFAULT NULL,
    `deleted_at`   datetime(6) DEFAULT NULL,
    `updated_at`   datetime(6) DEFAULT NULL,
    PRIMARY KEY (`id`)
);
INSERT INTO `customer`
VALUES (2, 'hospitad1', NULL, NULL, NULL);
INSERT INTO `customer`
VALUES (3, 'hospitad2', NULL, NULL, NULL);
CREATE TABLE `user`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT,
    `name`        varchar(255) DEFAULT NULL,
    `password`    varchar(255) DEFAULT NULL,
    `username`    varchar(255) DEFAULT NULL,
    `customer_id` bigint(20) DEFAULT NULL,
    `created_at`  datetime(6) DEFAULT NULL,
    `deleted_at`  datetime(6) DEFAULT NULL,
    `updated_at`  datetime(6) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY           `FKdptx0i3ky01svofwjytq5iry0` (`customer_id`),
    CONSTRAINT `FKdptx0i3ky01svofwjytq5iry0` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
);
INSERT INTO `user`
VALUES (2, 'arsalan', '$2a$10$UwfzkQl28x7Ps5YvwHr8eeGbYUTwiEizIIs/Q/Zva6INL.s9.Ajd2', 'arsalan1', 2, NULL, NULL, NULL);
INSERT INTO `user`
VALUES (3, 'arsalan', '$2a$10$UwfzkQl28x7Ps5YvwHr8eeGbYUTwiEizIIs/Q/Zva6INL.s9.Ajd2', 'arsalan2', 3, NULL, NULL, NULL);

CREATE TABLE `organization`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT,
    `name`        varchar(255) DEFAULT NULL,
    `creator_id`  bigint(20) DEFAULT NULL,
    `customer_id` bigint(20) DEFAULT NULL,
    `parent_id`   bigint(20) DEFAULT NULL,
    `created_at`  datetime(6) DEFAULT NULL,
    `deleted_at`  datetime(6) DEFAULT NULL,
    `updated_at`  datetime(6) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY           `FKhm8dk7ebyslwtp9hfkovhg5m5` (`creator_id`),
    KEY           `FK7eoprn0yt8npt9vk8t38tbfs` (`customer_id`),
    KEY           `FKc30yedjwp9qw1f3nn2ytda7tj` (`parent_id`),
    CONSTRAINT `FK7eoprn0yt8npt9vk8t38tbfs` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
    CONSTRAINT `FKc30yedjwp9qw1f3nn2ytda7tj` FOREIGN KEY (`parent_id`) REFERENCES `organization` (`id`),
    CONSTRAINT `FKhm8dk7ebyslwtp9hfkovhg5m5` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`)
);


INSERT INTO `organization`
VALUES (2, 'org1', 2, 2, NULL, '2021-01-28 13:47:25.046279', '2021-01-28 14:05:03.000000',
        '2021-01-28 13:47:25.046309'),
       (3, 'org2', 2, 2, NULL, '2021-01-28 13:49:23.441189', NULL, '2021-01-28 20:29:01.756168'),
       (4, 'org3', 2, 2, NULL, '2021-01-28 13:50:50.084870', NULL, '2021-01-28 13:50:50.084899'),
       (5, 'org4', 2, 2, NULL, '2021-01-28 13:53:26.484134', NULL, '2021-01-28 13:53:26.484181'),
       (6, 'org5', 2, 2, 5, '2021-01-28 13:56:27.008723', NULL, '2021-01-28 20:40:44.251796'),
       (7, 'org12', 2, 2, 3, '2021-01-28 14:04:49.119761', NULL, '2021-01-28 18:26:58.672280'),
       (8, 'org13', 2, 2, 7, '2021-01-28 20:33:35.701874', NULL, '2021-01-28 20:40:37.628282'),
       (9, 'org14', 2, 2, 7, '2021-01-28 20:33:41.674385', NULL, '2021-01-28 20:40:33.992575'),
       (10, 'org15', 2, 2, 7, '2021-01-28 20:33:45.505010', NULL, '2021-01-28 20:40:27.287826'),
       (11, 'org16', 2, 2, 7, '2021-01-28 20:33:48.921095', NULL, '2021-01-28 20:40:24.215892'),
       (12, 'org17', 2, 2, 7, '2021-01-28 20:33:53.315195', NULL, '2021-01-28 20:40:20.432392'),
       (13, 'org18', 2, 2, 7, '2021-01-28 20:33:57.131241', NULL, '2021-01-28 20:40:17.957840'),
       (14, 'org19', 2, 2, 7, '2021-01-28 20:34:00.932300', NULL, '2021-01-28 20:40:15.171719'),
       (15, 'org20', 2, 2, 7, '2021-01-28 20:34:05.202233', NULL, '2021-01-28 20:40:11.714845'),
       (16, 'org21', 2, 2, 7, '2021-01-28 20:34:09.884168', NULL, '2021-01-28 20:39:40.131193'),
       (17, 'org22', 2, 3, 7, '2021-01-28 20:34:13.110934', NULL, '2021-01-28 20:39:46.442262');


