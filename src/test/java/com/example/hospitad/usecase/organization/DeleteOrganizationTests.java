package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Organization;
import com.example.hospitad.entity.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.example.hospitad.persistence.repository.OrganizationRepository;
import com.example.hospitad.usecase.exception.OrganizationNotExist;
import com.example.hospitad.usecase.exception.UserNotAllowed;
import com.example.hospitad.usecase.user.CheckUserPermission;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class DeleteOrganizationTests {

  @MockBean CheckOrganizationExist checkOrganizationExist;

  @MockBean OrganizationRepository organizationRepository;

  @MockBean CheckUserPermission checkUserPermission;

  @Autowired DeleteOrganization deleteOrganization;

  @Test
  public void deleteOrganization_WhenOrganizationNotExist_ThrowException() {

    Organization mockOrganization = new Organization();
    mockOrganization.setId(1L);
    User user = new User();
    user.setId(1L);
    Mockito.when(checkOrganizationExist.isExistById(mockOrganization.getId())).thenReturn(false);
    assertThrows(
        OrganizationNotExist.class,
        () -> deleteOrganization.deleteByIdIfExist(mockOrganization.getId(), user));
  }

  @Test
  public void deleteOrganization_WhenUserHasNotPermission_ThrowException() {

    Organization mockOrganization = new Organization();
    mockOrganization.setId(1L);
    User user = new User();
    user.setId(1L);
    Mockito.when(checkOrganizationExist.isExistById(mockOrganization.getId())).thenReturn(true);
    Mockito.when(organizationRepository.findById(mockOrganization.getId()))
        .thenReturn(Optional.of(mockOrganization));
    Mockito.when(checkUserPermission.hasDeleteOrganizationPermission(mockOrganization, user))
        .thenReturn(false);
    assertThrows(
        UserNotAllowed.class,
        () -> deleteOrganization.deleteByIdIfExist(mockOrganization.getId(), user));
  }

  @Test
  public void deleteOrganization_Successful() throws OrganizationNotExist, UserNotAllowed {

    Organization mockOrganization = new Organization();
    mockOrganization.setId(1L);
    User user = new User();
    user.setId(1L);
    Mockito.when(checkOrganizationExist.isExistById(mockOrganization.getId())).thenReturn(true);
    Mockito.when(organizationRepository.findById(mockOrganization.getId()))
        .thenReturn(Optional.of(mockOrganization));
    Mockito.when(checkUserPermission.hasDeleteOrganizationPermission(mockOrganization, user))
        .thenReturn(true);
    deleteOrganization.deleteByIdIfExist(mockOrganization.getId(), user);
  }
}
