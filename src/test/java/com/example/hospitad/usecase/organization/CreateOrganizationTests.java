package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Customer;
import com.example.hospitad.entity.Organization;
import com.example.hospitad.entity.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.example.hospitad.persistence.repository.OrganizationRepository;
import com.example.hospitad.usecase.customer.GetCustomer;
import com.example.hospitad.usecase.exception.*;
import com.example.hospitad.usecase.user.GetUser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class CreateOrganizationTests {

  @MockBean CheckOrganizationExist checkOrganizationExist;

  @MockBean OrganizationRepository organizationRepository;

  @MockBean GetOrganization getOrganization;

  @MockBean GetUser getUser;

  @MockBean GetCustomer getCustomer;

  @Autowired CreateOrganization createOrganization;

  @Test
  public void createNewOrganization_Successful()
      throws OrganizationNotExist, CustomerNotExist, UserNotExist, OrganizationExist {

    Organization organization = new Organization();
    organization.setName("organization1");
    Organization parent = new Organization();
    parent.setName("parent");
    parent.setId(2L);
    organization.setParent(parent);
    User creator = new User();
    creator.setUsername("user1");
    creator.setId(1l);
    Customer customer = new Customer();
    customer.setId(1l);
    creator.setCustomer(customer);
    organization.setCreator(creator);
    organization.setId(1l);

    Mockito.when(
            checkOrganizationExist.isExistByCustomerIdAndName(customer.getId(), "organization1"))
        .thenReturn(false);
    Mockito.when(getOrganization.getById(parent.getId())).thenReturn(parent);
    Mockito.when(getCustomer.getById(customer.getId())).thenReturn(customer);
    Mockito.when(getUser.getById(creator.getId())).thenReturn(creator);
    Mockito.when(organizationRepository.save(any())).thenReturn(organization);
    Organization createdOrganization = createOrganization.createIfNotExist(organization);
    assertEquals(organization.getName(), createdOrganization.getName());
  }

  @Test
  public void createNewOrganization_WhenOrganizationNameIsDuplicateExist_ThrowException() {
    Organization organization = new Organization();
    organization.setName("organization1");
    Organization parent = new Organization();
    parent.setName("parent");
    parent.setId(2L);
    organization.setParent(parent);
    User creator = new User();
    creator.setUsername("user1");
    creator.setId(1l);
    Customer customer = new Customer();
    customer.setId(1l);
    creator.setCustomer(customer);
    organization.setCreator(creator);

    Mockito.when(
            checkOrganizationExist.isExistByCustomerIdAndName(customer.getId(), "organization1"))
        .thenReturn(true);
    assertThrows(OrganizationExist.class, () -> createOrganization.createIfNotExist(organization));
  }

  @Test
  public void createNewOrganization_ParentNotExist_ThrowException()
      throws OrganizationNotExist, CustomerNotExist, UserNotExist {
    Organization organization = new Organization();
    organization.setName("organization1");
    Organization parent = new Organization();
    parent.setName("parent");
    parent.setId(2L);
    organization.setParent(parent);
    User creator = new User();
    creator.setUsername("user1");
    creator.setId(1l);
    Customer customer = new Customer();
    customer.setId(1l);
    creator.setCustomer(customer);
    organization.setCreator(creator);

    Mockito.when(
            checkOrganizationExist.isExistByCustomerIdAndName(customer.getId(), "organization1"))
        .thenReturn(false);
    Mockito.when(getOrganization.getById(parent.getId())).thenThrow(ParentNotExist.class);
    Mockito.when(getCustomer.getById(customer.getId())).thenReturn(customer);
    Mockito.when(getUser.getById(creator.getId())).thenReturn(creator);
    Mockito.when(organizationRepository.save(organization)).thenReturn(organization);
    assertThrows(ParentNotExist.class, () -> createOrganization.createIfNotExist(organization));
  }
}
