package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Organization;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import com.example.hospitad.persistence.repository.OrganizationRepository;
import com.example.hospitad.usecase.exception.InvalidPageNumber;
import com.example.hospitad.usecase.exception.OrganizationNotExist;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class GetOrganizationTests {
  @MockBean OrganizationRepository organizationRepository;

  @Autowired GetOrganization getOrganization;

  @MockBean CheckOrganizationExist checkOrganizationExist;

  @Test
  public void getById_ReturnOrganization_WhenExist_ReturnOrganization()
      throws OrganizationNotExist {
    Organization mockOrganization = new Organization();
    mockOrganization.setId(1l);
    Mockito.when(organizationRepository.findById(1L))
        .thenReturn(java.util.Optional.of(mockOrganization));
    Organization found = getOrganization.getById(1L);
    assertEquals(found.getId(), mockOrganization.getId());
  }

  @Test
  public void getById_WhenNotExist_ThrowException() {
    Mockito.when(organizationRepository.findById(1L)).thenReturn(Optional.empty());
    assertThrows(OrganizationNotExist.class, () -> getOrganization.getById(1L));
  }

  @Test
  public void getOrganizationByPageNumber_ReturnOrganizationList() {
    List<Organization> allOrg = generateMockOrganizations(10);
    Page<Organization> organizationPage = new PageImpl<>(allOrg);
    Mockito.when(organizationRepository.findAll(PageRequest.of(0, 10)))
        .thenReturn((organizationPage));
    List<Organization> foundOrgs = getOrganization.getOrganizationListByPageNumber(1);
    assertEquals(foundOrgs.size(), 10);
    assertEquals(foundOrgs.get(0).getId(), allOrg.get(0).getId());
    assertEquals(foundOrgs.get(1).getId(), allOrg.get(1).getId());
  }

  @Test
  public void getOrganizationByPageNumber_WhenNotExist_ReturnEmptyOrganizationList() {
    List<Organization> allOrg = new ArrayList<>();
    Page<Organization> organizationPage = new PageImpl<>(allOrg);
    Mockito.when(organizationRepository.findAll(PageRequest.of(0, 10)))
        .thenReturn((organizationPage));
    List<Organization> foundOrgs = getOrganization.getOrganizationListByPageNumber(1);
    assertEquals(foundOrgs.size(), 0);
  }

  @Test
  public void
      getOrganizationChildrenById_WhenOrganizationExist_WhenPageNumberGreaterThanZero_ReturnChildrenList()
          throws OrganizationNotExist, InvalidPageNumber {
    List<Organization> allOrg = generateMockOrganizations(10);
    Mockito.when(organizationRepository.getByParentId(9L, PageRequest.of(0, 10)))
        .thenReturn(List.copyOf(allOrg.get(9).getChildren()));
    Mockito.when(checkOrganizationExist.isExistById(9L)).thenReturn(true);
    List<Organization> foundOrgs = getOrganization.getOrganizationChildrenListByPageNumber(9L, 1);
    assertEquals(8, foundOrgs.size());
  }

  @Test
  public void
      getOrganizationChildrenById_WhenOrganizationExist_WhenPageNumberLowerThanOne_ThrowException()
          throws OrganizationNotExist {
    List<Organization> allOrg = generateMockOrganizations(10);
    Mockito.when(organizationRepository.getByParentId(9L, PageRequest.of(0, 10)))
        .thenReturn(List.copyOf(allOrg.get(9).getChildren()));
    Mockito.when(checkOrganizationExist.isExistById(9L)).thenReturn(true);
    assertThrows(
        InvalidPageNumber.class,
        () -> getOrganization.getOrganizationChildrenListByPageNumber(9L, 0));
  }

  @Test
  public void
      getOrganizationChildrenById_WhenOrganizationNotExist_WhenPageNumberGreaterThanZero_ThrowException()
          throws OrganizationNotExist {
    List<Organization> allOrg = generateMockOrganizations(10);
    Mockito.when(organizationRepository.getByParentId(9L, PageRequest.of(0, 10)))
        .thenReturn(List.copyOf(allOrg.get(9).getChildren()));
    Mockito.when(checkOrganizationExist.isExistById(9L)).thenReturn(false);
    assertThrows(
        OrganizationNotExist.class,
        () -> getOrganization.getOrganizationChildrenListByPageNumber(9L, 1));
  }

  private List<Organization> generateMockOrganizations(int totalMockObject) {
    List<Organization> allOrg = new ArrayList();
    for (int i = 0; i < totalMockObject; i++) {
      allOrg.add(new Organization());
      allOrg.get(i).setId(Long.valueOf(i));
      allOrg.get(i).setName("Organization" + i);
    }
    for (int i = 1; i < totalMockObject; i++) {
      allOrg.get(i).setChildren(Set.copyOf(allOrg.subList(0, i - 1)));
    }
    return allOrg;
  }
}
