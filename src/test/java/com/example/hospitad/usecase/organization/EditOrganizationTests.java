package com.example.hospitad.usecase.organization;

import com.example.hospitad.entity.Customer;
import com.example.hospitad.entity.Organization;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.example.hospitad.persistence.repository.OrganizationRepository;
import com.example.hospitad.usecase.exception.OrganizationExist;
import com.example.hospitad.usecase.exception.OrganizationNotExist;
import com.example.hospitad.usecase.exception.ParentAndChildIsSame;
import com.example.hospitad.usecase.exception.ParentNotExist;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class EditOrganizationTests {

  @MockBean CheckOrganizationExist checkOrganizationExist;

  @MockBean OrganizationRepository organizationRepository;

  @Autowired EditOrganization editOrganization;

  @Test
  public void editInformation_WhenOrganizationNotExist_ThrowException() {
    Mockito.when(checkOrganizationExist.isExistById(1L)).thenReturn(false);
    Organization newData = generateNewOrganizationData();
    assertThrows(OrganizationNotExist.class, () -> editOrganization.changeInformation(1L, newData));
  }

  @Test
  public void editInformation_WhenNewNameIsAlreadyExist_ThrowException() {

    Optional<Organization> org = generateMockOrganizationObject();
    Organization newData = generateNewOrganizationData();
    Mockito.when(checkOrganizationExist.isExistById(1L)).thenReturn(true);
    Mockito.when(organizationRepository.findById(1L)).thenReturn(org);
    Mockito.when(checkOrganizationExist.isExistByCustomerIdAndName(1L, "newName")).thenReturn(true);

    assertThrows(OrganizationExist.class, () -> editOrganization.changeInformation(1L, newData));
  }

  @Test
  public void editInformation_WhenNewParentNotExist_ThrowException() {
    Organization newData = generateNewOrganizationData();
    Optional<Organization> org = generateMockOrganizationObject();
    Mockito.when(checkOrganizationExist.isExistById(1L)).thenReturn(true);
    Mockito.when(organizationRepository.findById(1L)).thenReturn(org);
    Mockito.when(checkOrganizationExist.isExistByCustomerIdAndName(1L, "newName"))
        .thenReturn(false);
    Mockito.when(checkOrganizationExist.isExistByCustomerIdAndName(1L, "parent")).thenReturn(false);

    assertThrows(ParentNotExist.class, () -> editOrganization.changeInformation(1L, newData));
  }

  @Test
  public void editInformation_WhenNewParentIsOrganizationWithNewData_ThrowException() {
    Optional<Organization> org = generateMockOrganizationObject();
    Organization newData = generateNewOrganizationData();
    newData.getParent().setName("oldName");
    org.get().setName("oldName");

    Mockito.when(checkOrganizationExist.isExistById(1L)).thenReturn(true);
    Mockito.when(organizationRepository.findById(1L)).thenReturn(org);
    Mockito.when(checkOrganizationExist.isExistByCustomerIdAndName(1L, "oldName")).thenReturn(true);
    Mockito.when(organizationRepository.findByCustomerIdAndName(1L, "oldName")).thenReturn(org);

    assertThrows(ParentAndChildIsSame.class, () -> editOrganization.changeInformation(1L, newData));
  }

  @Test
  public void editInformation_SaveNewInformation() {

    Organization newParent = new Organization();
    newParent.setName("parent");
    newParent.setId(2L);
    Optional<Organization> optionalParent = Optional.of(newParent);
    Optional<Organization> org = generateMockOrganizationObject();
    Mockito.when(checkOrganizationExist.isExistById(1L)).thenReturn(true);
    Mockito.when(organizationRepository.findById(1L)).thenReturn(org);
    Mockito.when(checkOrganizationExist.isExistByCustomerIdAndName(1L, "newName"))
        .thenReturn(false);
    Mockito.when(checkOrganizationExist.isExistByCustomerIdAndName(1L, "parent")).thenReturn(true);
    Mockito.when(organizationRepository.findByCustomerIdAndName(1L, "parent"))
        .thenReturn(optionalParent);
  }

  private Optional<Organization> generateMockOrganizationObject() {
    Organization newParent = new Organization();
    newParent.setName("parent");

    Organization newData = new Organization();
    newData.setName("newName");
    newData.setParent(newParent);

    Organization organization = new Organization();
    organization.setId(1L);
    organization.setName("oldName");
    Customer customer = new Customer();
    customer.setId(1L);
    organization.setCustomer(customer);
    Optional org = Optional.of(organization);
    return org;
  }

  private Organization generateNewOrganizationData() {
    Organization parent = new Organization();
    parent.setName("parent");
    Organization newData = new Organization();
    newData.setName("newName");
    newData.setParent(parent);
    return newData;
  }
}
