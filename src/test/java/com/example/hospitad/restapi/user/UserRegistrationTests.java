package com.example.hospitad.restapi.user;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;
import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.example.hospitad.adapter.restapi.UserController;
import com.example.hospitad.adapter.viewmodel.CustomerViewModel;
import com.example.hospitad.adapter.viewmodel.UserViewModel;
import com.example.hospitad.restapi.BaseRestAPITest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(value = UserController.class)
@OverrideAutoConfiguration(enabled = true)
@WithMockUser(username = "arsalan1")
@Sql(
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:test_db.sql"})
public class UserRegistrationTests extends BaseRestAPITest {

  @Autowired private ObjectMapper jacksonJSONConverter;

  @Test
  public void registerUser_Successful() throws Exception {

    UserViewModel newUser = new UserViewModel();
    newUser.setName("new user");
    newUser.setUsername("username");
    newUser.setPassword("123456");
    newUser.setMatchingPassword("123456");
    CustomerViewModel customer = new CustomerViewModel();
    customer.setCompanyName("new company");
    newUser.setCustomer(customer);

    ResponseMessageJsonPresenter response =
        getPostAPIResponse("/user/register/", jacksonJSONConverter.writeValueAsString(newUser));
    assertEquals(ResourceBundleUtil.getMessage("user.register_successful"), response.getMessage());
  }

  @Test
  public void registerUser_WhenNotMatchingPassword_ReturnException() throws Exception {

    UserViewModel newUser = new UserViewModel();
    newUser.setName("new user");
    newUser.setUsername("username");
    newUser.setPassword("123456");
    newUser.setMatchingPassword("1234567");
    CustomerViewModel customer = new CustomerViewModel();
    customer.setCompanyName("new company");
    newUser.setCustomer(customer);

    ResponseMessageJsonPresenter response =
        getPostAPIResponse("/user/register/", jacksonJSONConverter.writeValueAsString(newUser));
    assertEquals(ResourceBundleUtil.getMessage("user.password_not_match"), response.getMessage());
  }

  @Test
  public void registerUser_WhenDuplicateUserName_ReturnException() throws Exception {

    UserViewModel newUser = new UserViewModel();
    newUser.setName("new user");
    newUser.setUsername("arsalan1");
    newUser.setPassword("123456");
    newUser.setMatchingPassword("123456");
    CustomerViewModel customer = new CustomerViewModel();
    customer.setCompanyName("new company");
    newUser.setCustomer(customer);

    ResponseMessageJsonPresenter response =
        getPostAPIResponse("/user/register/", jacksonJSONConverter.writeValueAsString(newUser));
    assertEquals(ResourceBundleUtil.getMessage("user.exist"), response.getMessage());
  }
}
