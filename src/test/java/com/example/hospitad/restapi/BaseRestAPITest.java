package com.example.hospitad.restapi;

import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

public class BaseRestAPITest {

  @Autowired private MockMvc mvc;

  @Autowired private ObjectMapper jacksonJSONConverter;

  protected ResponseMessageJsonPresenter getPostAPIResponse(String url, String requestBody)
      throws Exception {
    MvcResult result =
        mvc.perform(post(url).contentType(MediaType.APPLICATION_JSON).content(requestBody))
            .andReturn();
    ResponseMessageJsonPresenter response =
        jacksonJSONConverter.readValue(
            result.getResponse().getContentAsString(), ResponseMessageJsonPresenter.class);
    return response;
  }

  protected ResponseMessageJsonPresenter getGetAPIResponse(String url) throws Exception {
    MvcResult result = mvc.perform(get(url).contentType(MediaType.APPLICATION_JSON)).andReturn();
    ResponseMessageJsonPresenter response =
        jacksonJSONConverter.readValue(
            result.getResponse().getContentAsString(), ResponseMessageJsonPresenter.class);
    return response;
  }

  protected ResponseMessageJsonPresenter getDeleteAPIResponse(String url) throws Exception {
    MvcResult result = mvc.perform(delete(url).contentType(MediaType.APPLICATION_JSON)).andReturn();
    ResponseMessageJsonPresenter response =
        jacksonJSONConverter.readValue(
            result.getResponse().getContentAsString(), ResponseMessageJsonPresenter.class);
    return response;
  }

  protected ResponseMessageJsonPresenter getPutAPIResponse(String url, String requestBody)
      throws Exception {
    MvcResult result =
        mvc.perform(put(url).contentType(MediaType.APPLICATION_JSON).content(requestBody))
            .andReturn();
    ResponseMessageJsonPresenter response =
        jacksonJSONConverter.readValue(
            result.getResponse().getContentAsString(), ResponseMessageJsonPresenter.class);
    return response;
  }
}
