package com.example.hospitad.restapi.organization;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;
import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.example.hospitad.adapter.restapi.OrganizationController;
import com.example.hospitad.restapi.BaseRestAPITest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.hospitad.entity.Organization;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(value = OrganizationController.class)
@OverrideAutoConfiguration(enabled = true)
@WithMockUser(username = "arsalan1")
@Sql(
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:test_db.sql"})
public class CreateOrganizationTests extends BaseRestAPITest {

  @Autowired private ObjectMapper jacksonJSONConverter;

  @Test
  public void createOrganization_Successful() throws Exception {
    Organization parent = new Organization();
    parent.setName("parent");
    parent.setId(3L);
    Organization organization = new Organization();
    organization.setParent(parent);
    organization.setName("organization 1");
    ResponseMessageJsonPresenter response =
        getPostAPIResponse("/organization/", jacksonJSONConverter.writeValueAsString(organization));
    assertEquals(organization.getName(), ((HashMap) response.getData()).get("name"));
  }

  @Test
  public void createOrganization_WhenParentNotExist_ReturnException() throws Exception {
    Organization parent = new Organization();
    parent.setName("parent");
    parent.setId(2L);
    Organization organization = new Organization();
    organization.setParent(parent);
    organization.setName("organization 1");
    ResponseMessageJsonPresenter response =
        getPostAPIResponse("/organization/", jacksonJSONConverter.writeValueAsString(organization));
    assertEquals(
        ResourceBundleUtil.getMessage("organization.parent.not_exist"), response.getMessage());
  }
}
