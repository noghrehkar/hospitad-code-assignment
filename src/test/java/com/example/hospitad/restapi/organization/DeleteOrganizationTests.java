package com.example.hospitad.restapi.organization;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;
import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.example.hospitad.adapter.restapi.OrganizationController;
import com.example.hospitad.restapi.BaseRestAPITest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(value = OrganizationController.class)
@OverrideAutoConfiguration(enabled = true)
@WithMockUser(username = "arsalan1")
@Sql(
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:test_db.sql"})
public class DeleteOrganizationTests extends BaseRestAPITest {

  @Test
  public void deleteOrganization_WhenOrganizationExist_Successful() throws Exception {
    ResponseMessageJsonPresenter response = getDeleteAPIResponse("/organization/3");
    assertEquals(
        ResourceBundleUtil.getMessage("organization.delete_successful"), response.getMessage());
  }

  @Test
  public void deleteOrganization_WhenOrganizationNotExist_ReturnException() throws Exception {
    ResponseMessageJsonPresenter response = getDeleteAPIResponse("/organization/2");
    assertEquals(ResourceBundleUtil.getMessage("organization.not_exist"), response.getMessage());
  }

  @WithMockUser(username = "arsalan2")
  @Test
  public void deleteOrganization_WhenUserHasNotPermission_ReturnException() throws Exception {
    ResponseMessageJsonPresenter response = getDeleteAPIResponse("/organization/3");
    assertEquals(ResourceBundleUtil.getMessage("user.not_allowed"), response.getMessage());
  }
}
