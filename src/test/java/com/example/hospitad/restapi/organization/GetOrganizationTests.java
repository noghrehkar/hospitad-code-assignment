package com.example.hospitad.restapi.organization;

import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.example.hospitad.adapter.restapi.OrganizationController;
import com.example.hospitad.restapi.BaseRestAPITest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(value = OrganizationController.class)
@OverrideAutoConfiguration(enabled = true)
@WithMockUser
@Sql(
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:test_db.sql"})
public class GetOrganizationTests extends BaseRestAPITest {

  @Test
  public void getAllOrganization_WhenPageNumberOne_ReturnList() throws Exception {

    ResponseMessageJsonPresenter response = getGetAPIResponse("/organization?pageNumber=1");
    assertEquals(10, List.copyOf((List) response.getData()).size());
  }

  @Test
  public void getAllOrganization_WhenPageNumberTwo_ReturnList() throws Exception {

    ResponseMessageJsonPresenter response = getGetAPIResponse("/organization?pageNumber=2");
    assertEquals(5, List.copyOf((List) response.getData()).size());
  }

  @Test
  public void getAllOrganization_ChildrenPageOne_ReturnList() throws Exception {

    ResponseMessageJsonPresenter response =
        getGetAPIResponse("/organization/7/children?pageNumber=1");
    assertEquals(10, List.copyOf((List) response.getData()).size());
  }

  @Test
  public void getAllOrganization_ChildrenPageTwo_ReturnEmptyList() throws Exception {

    ResponseMessageJsonPresenter response =
        getGetAPIResponse("/organization/7/children?pageNumber=2");
    assertEquals(0, List.copyOf((List) response.getData()).size());
  }
}
