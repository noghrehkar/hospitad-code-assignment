package com.example.hospitad.restapi.organization;

import com.example.hospitad.adapter.presenter.ResourceBundleUtil;
import com.example.hospitad.adapter.presenter.ResponseMessageJsonPresenter;
import com.example.hospitad.adapter.restapi.OrganizationController;
import com.example.hospitad.restapi.BaseRestAPITest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.hospitad.entity.Organization;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(value = OrganizationController.class)
@OverrideAutoConfiguration(enabled = true)
@WithMockUser
@Sql(
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:test_db.sql"})
public class EditOrganizationTests extends BaseRestAPITest {

  @Autowired private ObjectMapper jacksonJSONConverter;

  @Test
  public void editOrganization_Successful() throws Exception {

    Organization parent = new Organization();
    parent.setName("org3");
    parent.setId(4L);
    Organization newData = new Organization();
    newData.setParent(parent);
    newData.setName("organization 1");

    ResponseMessageJsonPresenter response =
        getPutAPIResponse("/organization/5", jacksonJSONConverter.writeValueAsString(newData));
    assertEquals(
        ResourceBundleUtil.getMessage("organization.edit_successful"), response.getMessage());
  }

  @Test
  public void editOrganization_WhenSameParentIsSameAsChild_ReturnException() throws Exception {

    Organization parent = new Organization();
    parent.setName("org3");
    parent.setId(4L);
    Organization newData = new Organization();
    newData.setParent(parent);
    newData.setName("organization 1");

    ResponseMessageJsonPresenter response =
        getPutAPIResponse("/organization/4", jacksonJSONConverter.writeValueAsString(newData));
    assertEquals(
        ResourceBundleUtil.getMessage("organization.parent_child_is_same"), response.getMessage());
  }

  @Test
  public void editOrganization_WhenParentNotExist_ReturnException() throws Exception {
    Organization parent = new Organization();
    parent.setName("new organization");
    parent.setId(20L);
    Organization newData = new Organization();
    newData.setParent(parent);
    newData.setName("organization 1");

    ResponseMessageJsonPresenter response =
        getPutAPIResponse("/organization/4", jacksonJSONConverter.writeValueAsString(newData));
    assertEquals(
        ResourceBundleUtil.getMessage("organization.parent.not_exist"), response.getMessage());
  }

  @Test
  public void editOrganization_WhenSameNameAlreadyExist_ReturnException() throws Exception {

    Organization parent = new Organization();
    parent.setName("org3");
    parent.setId(4L);
    Organization newData = new Organization();
    newData.setParent(parent);
    newData.setName("org4");

    ResponseMessageJsonPresenter response =
        getPutAPIResponse("/organization/5", jacksonJSONConverter.writeValueAsString(newData));
    assertEquals(ResourceBundleUtil.getMessage("organization.exist"), response.getMessage());
  }

  @Test
  public void editOrganization_WhenSameNameWithDifferentCustomer_Successful() throws Exception {

    Organization parent = new Organization();
    parent.setName("org3");
    parent.setId(4L);
    Organization newData = new Organization();
    newData.setParent(parent);
    newData.setName("org22");

    ResponseMessageJsonPresenter response =
        getPutAPIResponse("/organization/5", jacksonJSONConverter.writeValueAsString(newData));
    assertEquals(
        ResourceBundleUtil.getMessage("organization.edit_successful"), response.getMessage());
  }
}
